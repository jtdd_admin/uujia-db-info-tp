<?php


namespace uujia\dbinfo\databases;


use think\Exception;
use think\facade\Config;
use think\facade\Db;
use uujia\dbinfo\exception\EDbInfoException;

class Databases {
	
	/**
	 * @var DbInfoLoader
	 */
	protected $_dbInfoLoader;
	
	/**
	 * @var Tables[]
	 */
	protected $_tables = [];
	
	
	/**
	 * 创建实例
	 * @return Databases|object
	 */
	public static function NewCreate(DbInfoLoader $dbInfoLoader = null) {
		// 反射构建实例化
		$reflection = new \ReflectionClass(static::class);
		return $reflection->newInstanceArgs(func_get_args());// 传入的是关联数组
	}
	
	/**
	 * Databases constructor.
	 *
	 * @param DbInfoLoader|null $dbInfoLoader
	 */
	public function __construct(DbInfoLoader $dbInfoLoader = null) {
		$this->_dbInfoLoader = $dbInfoLoader;
	}
	
	/**
	 * 加载表和字段
	 *
	 * Date: 2021/11/20
	 * Time: 18:19
	 *
	 * @param bool $reload
	 *
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\DbException
	 * @throws \think\db\exception\ModelNotFoundException
	 */
	public function load($database = '', $reload = false) {
		if (empty($database)) {
			$database = $this->_dbInfoLoader->getCurrDatabaseName();
		}
		
		// SELECT
		//     A.TABLE_SCHEMA '数据库',
		//     A.TABLE_NAME '表名',
		//     A.COLUMN_NAME '表列名',
		//     A.ORDINAL_POSITION '表字段顺序号',
		//     A.DATA_TYPE '列的数据类型',
		//     A.COLUMN_TYPE '列的类型',
		//     A.COLUMN_COMMENT '列的备注'
		// FROM INFORMATION_SCHEMA.COLUMNS A
		// WHERE A.TABLE_SCHEMA='数据库'
		// ORDER BY A.TABLE_SCHEMA,A.TABLE_NAME,A.ORDINAL_POSITION
		
		$data = Db::table('INFORMATION_SCHEMA.COLUMNS')
		          ->alias('A')
		          ->field('A.TABLE_SCHEMA,
				           A.TABLE_NAME,
				           A.COLUMN_NAME,
				           A.ORDINAL_POSITION,
				           A.DATA_TYPE,
				           A.COLUMN_TYPE,
				           A.COLUMN_COMMENT')
		          ->where('A.TABLE_SCHEMA', '=', $database)
		          ->order(['A.TABLE_SCHEMA', 'A.TABLE_NAME', 'A.ORDINAL_POSITION'])
		          ->select();
		
		foreach ($data as $i => $item) {
			// table
			if (empty($this->_tables[$item['TABLE_NAME']])) {
				$table = Tables::NewCreate($this->_dbInfoLoader, $this);
				$table->setData(array_merge($item, ['_index' => $this->tableCount()]));
				
				$this->_tables[$item['TABLE_NAME']] = $table;
			}
			
			// field
			$field = Fields::NewCreate($this->_dbInfoLoader, $this, $this->_tables[$item['TABLE_NAME']]);
			$field->setData(array_merge($item, ['_index' => $this->_tables[$item['TABLE_NAME']]->fieldCount()]));
			
			$this->_tables[$item['TABLE_NAME']]->fields()[$item['COLUMN_NAME']] = $field;
		}
		
		return $this;
	}
	
	/**
	 * 加载表
	 *
	 * Date: 2021/11/20
	 * Time: 18:19
	 *
	 * @param bool $reload
	 */
	public function loadTable($database = '', $reload = false) {
		if (empty($database)) {
			$database = $this->_dbInfoLoader->getCurrDatabaseName();
		}
		
		// SELECT
		//     A.TABLE_SCHEMA '数据库',
		//     A.TABLE_NAME '表名',
		//     A.COLUMN_NAME '表列名',
		//     A.ORDINAL_POSITION '表字段顺序号',
		//     A.DATA_TYPE '列的数据类型',
		//     A.COLUMN_TYPE '列的类型',
		//     A.COLUMN_COMMENT '列的备注'
		// FROM INFORMATION_SCHEMA.COLUMNS A
		// WHERE A.TABLE_SCHEMA='数据库'
		// ORDER BY A.TABLE_SCHEMA,A.TABLE_NAME,A.ORDINAL_POSITION
		
		$data = Db::table('INFORMATION_SCHEMA.COLUMNS')
		          ->alias('A')
		          ->distinct()
		          ->field('A.TABLE_SCHEMA, A.TABLE_NAME')
		          ->where('A.TABLE_SCHEMA', '=', $database)
		          ->order(['A.TABLE_SCHEMA', 'A.TABLE_NAME', 'A.ORDINAL_POSITION'])
		          ->select();
		
		foreach ($data as $i => $item) {
			// table
			if (empty($this->_tables[$item['TABLE_NAME']])) {
				$table = Tables::NewCreate($this->_dbInfoLoader, $this);
				$table->setData(array_merge($item, ['_index' => $this->tableCount()]));
				
				$this->_tables[$item['TABLE_NAME']] = $table;
			}
		}
		
		return $this;
	}
	
	/**
	 * 表数量
	 *
	 * Date: 2021/11/22
	 * Time: 2:11
	 *
	 * @return int
	 */
	public function tableCount() {
		return count($this->_tables);
	}
	
	/**
	 * @return DbInfoLoader
	 */
	public function getDbInfoLoader(): DbInfoLoader {
		return $this->_dbInfoLoader;
	}
	
	/**
	 * @param DbInfoLoader $dbInfoLoader
	 *
	 * @return Databases
	 */
	public function setDbInfoLoader(DbInfoLoader $dbInfoLoader) {
		$this->_dbInfoLoader = $dbInfoLoader;
		
		return $this;
	}
	
	/**
	 * 表
	 *
	 * @return Tables[]
	 */
	public function &tables() {
		return $this->_tables;
	}
	
}