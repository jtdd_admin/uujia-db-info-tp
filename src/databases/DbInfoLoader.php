<?php


namespace uujia\dbinfo\databases;


use think\facade\Config;
use think\facade\Db;
use uujia\dbinfo\exception\EDbInfoException;

class DbInfoLoader {
	/**
	 * @var Databases[]
	 */
	protected $_databases = [];
	
	/**
	 * 当前配置的数据库
	 * @var string
	 */
	protected $_currDatabaseName = '';
	
	/**
	 * DbInfoLoader constructor.
	 */
	public function __construct() {
		$this->init();
	}
	
	/**
	 * init
	 *
	 * Date: 2021/11/20
	 * Time: 19:04
	 *
	 * @return DbInfoLoader
	 * @throws EDbInfoException
	 */
	public function init() {
		$this->loadConfig();
		
		return $this;
	}
	
	/**
	 * 数据库
	 *
	 * @param bool $reload
	 *
	 * @return $this
	 * @throws EDbInfoException
	 */
	public function loadConfig($reload = false) {
		$databaseConfig = Config::get('database', []);
		if (empty($databaseConfig)) {
			throw new EDbInfoException('Database config is empty or error!');
		}
		
		$databaseConfigDefault = $databaseConfig['default'] ?? 'mysql';
		$databaseConfigConn = $databaseConfig['connections'][$databaseConfigDefault] ?? [];
		
		$this->_currDatabaseName = $databaseConfigConn['database'] ?? '';
		if (empty($databaseConfig)) {
			throw new EDbInfoException('Database name config is empty or error!');
		}
		
		return $this;
	}
	
	/**
	 * load
	 *
	 * Date: 2021/11/20
	 * Time: 19:34
	 *
	 * @return DbInfoLoader
	 */
	public function load() {
		$dbObj = Databases::NewCreate($this);
		$dbObj->load();
		
		$this->_databases[$this->getCurrDatabaseName()] = $dbObj;
		
		return $this;
	}
	
	/**
	 * 获取所有数据库
	 *
	 * Date: 2021/11/14
	 * Time: 2:34
	 *
	 * @param string $like
	 *
	 * @return array
	 */
	public function getDatabaseNames($like = '') {
		$sql = 'SHOW DATABASES';
		if (!empty($like)) {
			$sql .= " LIKE '{$like}'";
		}
		
		$databases = Db::query($sql);
		
		return $databases;
	}
	
	
	/**
	 * @return Databases[]
	 */
	public function &databases(): array {
		return $this->_databases;
	}
	
	/**
	 * 当前数据库对象
	 *
	 * Date: 2021/11/20
	 * Time: 19:11
	 *
	 * @return Databases
	 */
	public function currDatabase() {
		return $this->_databases[$this->_currDatabaseName];
	}
	
	/**
	 * @return Databases[]
	 */
	public function &getDatabases(): array {
		return $this->_databases;
	}
	
	/**
	 * @param Databases[] $databases
	 *
	 * @return DbInfoLoader
	 */
	public function setDatabases(array $databases) {
		$this->_databases = $databases;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCurrDatabaseName(): string {
		return $this->_currDatabaseName;
	}
	
	/**
	 * @param string $currDatabaseName
	 *
	 * @return DbInfoLoader
	 */
	public function setCurrDatabaseName(string $currDatabaseName) {
		$this->_currDatabaseName = $currDatabaseName;
		
		return $this;
	}
	
	
}