<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
namespace uujia\dbinfo;

use think\Route;
use think\Service as BaseService;

class Service extends BaseService {
	
	public function boot(Route $route) {
		$route->get('uujia/dbinfo/welcome', function () {
			return 'Welcome to uujia.';
		});
	}
	
	public function register() {
		$this->app->bind('uu_db_info', DbInfo::class);
	}
}
