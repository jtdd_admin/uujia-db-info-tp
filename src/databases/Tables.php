<?php


namespace uujia\dbinfo\databases;


class Tables {
	
	/**
	 * @var DbInfoLoader
	 */
	protected $_dbInfoLoader;
	
	/**
	 * @var Databases
	 */
	protected $_currDatabase;
	
	/**
	 * @var Fields[]
	 */
	protected $_fields = [];
	
	/**
	 * @var array
	 */
	protected $_data = [];
	
	/**
	 * 所在序号
	 * @var int
	 */
	protected $_index = 0;
	
	/**
	 * 数据库
	 * @var string
	 */
	protected $_table_schema;
	
	/**
	 * 表名
	 * @var string
	 */
	protected $_table_name;
	
	
	/**
	 * 创建实例
	 * @return Tables|object
	 */
	public static function NewCreate(DbInfoLoader $dbInfoLoader = null, Databases $currDatabase = null) {
		// 反射构建实例化
		$reflection = new \ReflectionClass(static::class);
		return $reflection->newInstanceArgs(func_get_args());// 传入的是关联数组
	}
	
	/**
	 * Tables constructor.
	 *
	 * @param DbInfoLoader|null $dbInfoLoader
	 * @param Databases|null    $currDatabase
	 */
	public function __construct(DbInfoLoader $dbInfoLoader = null, Databases $currDatabase = null) {
		$this->_dbInfoLoader = $dbInfoLoader;
		$this->_currDatabase = $currDatabase;
	}
	
	public function load() {
		return $this;
	}
	
	/**
	 * 字段数量
	 *
	 * Date: 2021/11/22
	 * Time: 2:01
	 *
	 * @return int
	 */
	public function fieldCount() {
		return count($this->_fields);
	}
	
	/**
	 * @return Databases[]
	 */
	public function getDatabases() {
		return $this->_dbInfoLoader->databases();
	}
	
	/**
	 * @return array
	 */
	public function getData(): array {
		return $this->_data;
	}
	
	/**
	 * @param array $data
	 *
	 * @return Tables
	 */
	public function setData(array $data) {
		$this->_data = $data;
		
		$this->_index = $data['_index'] ?? 0;
		$this->_table_schema = $data['TABLE_SCHEMA'] ?? '';
		$this->_table_name = $data['TABLE_NAME'] ?? '';
		
		return $this;
	}
	
	/**
	 * @return Databases
	 */
	public function getCurrDatabase(): Databases {
		return $this->_currDatabase;
	}
	
	/**
	 * @param Databases $currDatabase
	 *
	 * @return Tables
	 */
	public function _setCurrDatabase(Databases $currDatabase) {
		$this->_currDatabase = $currDatabase;
		
		return $this;
	}
	
	/**
	 * @return Fields[]
	 */
	public function &fields(): array {
		return $this->_fields;
	}
	
	/**
	 * @return Fields[]
	 */
	public function &getFields(): array {
		return $this->_fields;
	}
	
	/**
	 * @param Fields[] $fields
	 *
	 * @return Tables
	 */
	public function setFields(array $fields) {
		$this->_fields = $fields;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTableSchema(): string {
		return $this->_table_schema;
	}
	
	/**
	 * @param string $table_schema
	 *
	 * @return Tables
	 */
	public function setTableSchema(string $table_schema) {
		$this->_table_schema = $table_schema;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTableName(): string {
		return $this->_table_name;
	}
	
	/**
	 * @param string $table_name
	 *
	 * @return Tables
	 */
	public function setTableName(string $table_name) {
		$this->_table_name = $table_name;
		
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getIndex(): int {
		return $this->_index;
	}
	
	/**
	 * @param int $index
	 *
	 * @return Tables
	 */
	public function setIndex(int $index) {
		$this->_index = $index;
		
		return $this;
	}
	
}