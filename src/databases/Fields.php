<?php


namespace uujia\dbinfo\databases;


class Fields {
	
	/**
	 * @var DbInfoLoader
	 */
	protected $_dbInfoLoader;
	
	/**
	 * @var Databases
	 */
	protected $_currDatabase;
	
	/**
	 * @var Tables
	 */
	protected $_currTable;
	
	/**
	 * @var array
	 */
	protected $_data = [];
	
	/**
	 * 所在序号
	 * @var int
	 */
	protected $_index = 0;
	
	/**
	 * 表列名
	 * @var string
	 */
	protected $_column_name;
	
	/**
	 * 表字段顺序号
	 * @var int
	 */
	protected $_ordinal_position;
	
	/**
	 * 列的数据类型 varchar
	 * @var string
	 */
	protected $_data_type;
	
	/**
	 * 列的类型 varchar(200)
	 * @var string
	 */
	protected $_column_type;
	
	/**
	 * 列的备注
	 * @var string
	 */
	protected $_column_comment;
	
	/**
	 * 创建实例
	 * @return Fields|object
	 */
	public static function NewCreate(DbInfoLoader $dbInfoLoader = null,
	                                 Databases $currDatabase = null, Tables $currTable = null) {
		// 反射构建实例化
		$reflection = new \ReflectionClass(static::class);
		return $reflection->newInstanceArgs(func_get_args());// 传入的是关联数组
	}
	
	/**
	 * Fields constructor.
	 *
	 * @param DbInfoLoader|null $dbInfoLoader
	 * @param Databases|null    $currDatabase
	 */
	public function __construct(DbInfoLoader $dbInfoLoader = null,
	                            Databases $currDatabase = null, Fields $currTable = null) {
		$this->_dbInfoLoader = $dbInfoLoader;
		$this->_currDatabase = $currDatabase;
		$this->_currTable = $currTable;
	}
	
	public function load() {
		return $this;
	}
	
	/**
	 * @return DbInfoLoader
	 */
	public function getDbInfoLoader(): DbInfoLoader {
		return $this->_dbInfoLoader;
	}
	
	/**
	 * @param DbInfoLoader $dbInfoLoader
	 *
	 * @return Fields
	 */
	public function setDbInfoLoader(DbInfoLoader $dbInfoLoader) {
		$this->_dbInfoLoader = $dbInfoLoader;
		
		return $this;
	}
	
	/**
	 * @return Databases
	 */
	public function getCurrDatabase(): ?Databases {
		return $this->_currDatabase;
	}
	
	/**
	 * @param Databases|null $currDatabase
	 *
	 * @return Fields
	 */
	public function setCurrDatabase(?Databases $currDatabase) {
		$this->_currDatabase = $currDatabase;
		
		return $this;
	}
	
	/**
	 * @return Tables
	 */
	public function getCurrTable(): ?Tables {
		return $this->_currTable;
	}
	
	/**
	 * @param Tables|null $currTable
	 *
	 * @return Fields
	 */
	public function setCurrTable(?Tables $currTable) {
		$this->_currTable = $currTable;
		
		return $this;
	}
	
	/**
	 * @return Databases[]
	 */
	public function getDatabases() {
		return $this->_dbInfoLoader->databases();
	}
	
	/**
	 * @return Tables[]
	 */
	public function getTables() {
		return $this->_currDatabase->tables();
	}
	
	/**
	 * @return array
	 */
	public function &getData(): array {
		return $this->_data;
	}
	
	/**
	 * @param array $data
	 *
	 * @return Fields
	 */
	public function setData(array $data) {
		$this->_data = $data;
		
		$this->_index = $data['_index'] ?? 0;
		$this->_column_name = $data['COLUMN_NAME'] ?? '';
		$this->_ordinal_position = $data['ORDINAL_POSITION'] ?? 0;
		$this->_data_type = $data['DATA_TYPE'] ?? '';
		$this->_column_type = $data['COLUMN_TYPE'] ?? '';
		$this->_column_comment = $data['COLUMN_COMMENT'] ?? '';
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getColumnName(): string {
		return $this->_column_name;
	}
	
	/**
	 * @param string $column_name
	 *
	 * @return Fields
	 */
	public function setColumnName(string $column_name) {
		$this->_column_name = $column_name;
		
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getOrdinalPosition(): int {
		return $this->_ordinal_position;
	}
	
	/**
	 * @param int $ordinal_position
	 *
	 * @return Fields
	 */
	public function setOrdinalPosition(int $ordinal_position) {
		$this->_ordinal_position = $ordinal_position;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDataType(): string {
		return $this->_data_type;
	}
	
	/**
	 * @param string $data_type
	 *
	 * @return Fields
	 */
	public function setDataType(string $data_type) {
		$this->_data_type = $data_type;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getColumnComment(): string {
		return $this->_column_comment;
	}
	
	/**
	 * @param string $column_comment
	 *
	 * @return Fields
	 */
	public function setColumnComment(string $column_comment) {
		$this->_column_comment = $column_comment;
		
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getIndex(): int {
		return $this->_index;
	}
	
	/**
	 * @param int $index
	 *
	 * @return Fields
	 */
	public function setIndex(int $index) {
		$this->_index = $index;
		
		return $this;
	}
	
	
}