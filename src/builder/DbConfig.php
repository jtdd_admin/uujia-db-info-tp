<?php

namespace uujia\builder;


use utils\traits\InstanceTrait;

class DbConfig {
	use InstanceTrait;
	
	/**
	 * 配置
	 * @var array
	 */
	protected $_config = [];
	
	/**
	 * 获取数据库配置
	 *
	 * @return DbConfig
	 */
	public function load() {
		$default = config('database.default');
		
		$config = config("database.connections.{$default}");
		
		if (0 == $config['deploy']) {
			$dbConfig = [
				'adapter'      => $config['type'],
				'host'         => $config['hostname'],
				'name'         => $config['database'],
				'user'         => $config['username'],
				'pass'         => $config['password'],
				'port'         => $config['hostport'],
				'charset'      => $config['charset'],
				'table_prefix' => $config['prefix'],
			];
		} else {
			$dbConfig = [
				'adapter'      => explode(',', $config['type'])[0],
				'host'         => explode(',', $config['hostname'])[0],
				'name'         => explode(',', $config['database'])[0],
				'user'         => explode(',', $config['username'])[0],
				'pass'         => explode(',', $config['password'])[0],
				'port'         => explode(',', $config['hostport'])[0],
				'charset'      => explode(',', $config['charset'])[0],
				'table_prefix' => explode(',', $config['prefix'])[0],
			];
		}
		
		$table = config('database.migration_table', 'migrations');
		
		// $dbConfig['default_migration_table'] = $dbConfig['table_prefix'] . $table;
		$dbConfig['default_migration_table'] = 'app';
		
		$this->_config = $dbConfig;
		
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function &getConfig(): array {
		return $this->_config;
	}
	
	/**
	 * @param array $config
	 *
	 * @return DbConfig
	 */
	public function setConfig(array $config) {
		$this->_config = $config;
		
		return $this;
	}
	
}